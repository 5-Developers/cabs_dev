﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class TravelTimingRepository : IRepository<TravelTimingModels>
    {

        public IEnumerable<TravelTimingModels> Get()
        {
            try
            {
                List<TravelTimingModels> l = new List<TravelTimingModels>();
                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<TravelTimingModels> Get(TravelTimingModels model)
        {

            try
            {
                List<TravelTimingModels> l = new List<TravelTimingModels>();

                List<SqlParameter> ParameterList = new List<SqlParameter>();
                ParameterList.Add(DataAccess.GetDBParameter("@MONTH", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, model.Month));
                ParameterList.Add(DataAccess.GetDBParameter("@YEAR", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, model.Year));
                ParameterList.Add(DataAccess.GetDBParameter("@FROMDATE", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, model.FromDate));
                ParameterList.Add(DataAccess.GetDBParameter("@TODATE", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, model.ToDate));
                DataTable dt = DataAccess.ExecSPReturnDataTable("PRSCHEDULETRAVELTIMINGS", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new TravelTimingModels
                     {
                         TravelDate = row["FLDDATE"].ToString(),
                         RowSpan = int.Parse(row["FLDCOUNT"].ToString()),
                         Details = row["FLDDATA"].ToString(),
                         Message = ""

                     }).ToList();

                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TravelTimingModels Get(int Id)
        {
            TravelTimingModels m = new TravelTimingModels();
            return m;
        }

        public TravelTimingModels Get(Guid Id)
        {
            TravelTimingModels m = new TravelTimingModels();            
            return m;
        }

        public void Save(TravelTimingModels model)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }
    }
}