﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class ScheduleTravelRepository : IRepository<ScheduleTravelModel>
    {
        public IEnumerable<ScheduleTravelModel> Get()
        {
            try
            {
                List<ScheduleTravelModel> l = new List<ScheduleTravelModel>();
                List<SqlParameter> ParameterList = new List<SqlParameter>();
                DataTable dt = DataAccess.ExecSPReturnDataTable("PRSCHEDULETRAVELLIST", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new ScheduleTravelModel
                     {
                         ScheduleTravelId = Guid.Parse(row["FLDSCHEDULETRAVELID"].ToString()),
                         StDate = String.Format("{0:dd/MM/yyyy}", row["FLDTRAVELDATE"]),
                         StTime = String.Format("{0:HH:mm}", row["FLDTRAVELSTARTTIME"]),
                         FromLocation = row["FLDFROMLOCATION"].ToString(),
                         ToLocation = row["FLDTOLOCATION"].ToString(),
                         KmT = row["FLDKMTRAVELLED"].ToString(),
                         Remarks = row["FLDREMARKS"].ToString(),
                         InTime = row["FLDINTIME"].ToString(),
                         OutTime = row["FLDOUTTIME"].ToString(),
                         Status = row["FLDSTATUS"].ToString(),
                         CarName = row["FLDCARNAME"].ToString(),
                         DriverName = row["FLDDRIVERNAME"].ToString(),
                         DriverContactNo = row["FLDDRIVERCONTACTNO"].ToString(),
                         CarId = Guid.Parse(row["FLDCARID"].ToString()),
                         Message = ""
                     }).ToList();


                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ScheduleTravelModel> Get(ScheduleTravelModel model)
        {
            List<ScheduleTravelModel> l = new List<ScheduleTravelModel>();

            return (IEnumerable<ScheduleTravelModel>)l;
        }

        public ScheduleTravelModel Get(int Id)
        {
            ScheduleTravelModel m = new ScheduleTravelModel();
            return m;
        }

        public ScheduleTravelModel Get(Guid Id)
        {
            try
            {
                ScheduleTravelModel ScheduleTravel = new ScheduleTravelModel();
                List<SqlParameter> ParameterList = new List<SqlParameter>();
                ParameterList.Add(DataAccess.GetDBParameter("@SCHEDULETRAVELID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, Id));
                DataTable dt = DataAccess.ExecSPReturnDataTable("PRSCHEDULETRAVELEDIT", ParameterList);

                if (dt.Rows.Count > 0)
                {
                    ScheduleTravel.ScheduleTravelId = Guid.Parse(dt.Rows[0]["FLDSCHEDULETRAVELID"].ToString());
                    ScheduleTravel.StDate = String.Format("{0:dd/MM/yyyy}", dt.Rows[0]["FLDTRAVELDATE"]);
                    ScheduleTravel.StTime = String.Format("{0:HH:mm}", dt.Rows[0]["FLDTRAVELSTARTTIME"]);
                    ScheduleTravel.FromLocation = dt.Rows[0]["FLDFROMLOCATION"].ToString();
                    ScheduleTravel.ToLocation = dt.Rows[0]["FLDTOLOCATION"].ToString();
                    ScheduleTravel.KmT = dt.Rows[0]["FLDKMTRAVELLED"].ToString();
                    ScheduleTravel.Remarks = dt.Rows[0]["FLDREMARKS"].ToString();
                    ScheduleTravel.InTime = dt.Rows[0]["FLDINTIME"].ToString();
                    ScheduleTravel.OutTime = dt.Rows[0]["FLDOUTTIME"].ToString();
                    ScheduleTravel.Status = dt.Rows[0]["FLDSTATUS"].ToString();
                    ScheduleTravel.CarName = dt.Rows[0]["FLDCARNAME"].ToString();
                    ScheduleTravel.RegNo = dt.Rows[0]["FLDREGISTRATIONNO"].ToString();
                    ScheduleTravel.DriverName = dt.Rows[0]["FLDDRIVERNAME"].ToString();
                    ScheduleTravel.DriverContactNo = dt.Rows[0]["FLDDRIVERCONTACTNO"].ToString();
                    ScheduleTravel.CarId = Guid.Parse(dt.Rows[0]["FLDCARID"].ToString());
                    ScheduleTravel.Message = "";
                }

                return ScheduleTravel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(ScheduleTravelModel model)
        {
            try
            {
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@TRAVELDATE", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, model.TravelDate));
                ParameterList.Add(DataAccess.GetDBParameter("@TRAVELSTARTTIME", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, model.TravelTime));
                ParameterList.Add(DataAccess.GetDBParameter("@FROMLOCATION", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.FromLocation));
                ParameterList.Add(DataAccess.GetDBParameter("@TOLOCATION", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.ToLocation));
                ParameterList.Add(DataAccess.GetDBParameter("@DRIVERNAME", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.DriverName));
                ParameterList.Add(DataAccess.GetDBParameter("@DRIVERCONTACTNO", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.DriverContactNo));
                ParameterList.Add(DataAccess.GetDBParameter("@REMARKS", SqlDbType.NVarChar, DbConstant.VARCHAR_MAX, ParameterDirection.Input, model.Remarks));
                ParameterList.Add(DataAccess.GetDBParameter("@CARID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, model.CarId));
                ParameterList.Add(DataAccess.GetDBParameter("@USERID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, SecurityContext.CurrentSecurityContext.LoginId));


                if (model.hid == string.Empty)
                {
                    DataAccess.ExecSPReturnInt("PRSCHEDULETRAVEL", ParameterList);
                }
                else
                {
                    ParameterList.Add(DataAccess.GetDBParameter("@SCHEDULETRAVELID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, Guid.Parse(model.hid)));
                    DataAccess.ExecSPReturnInt("PRSCHEDULETRAVELUPDATE", ParameterList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void SaveInOutTime(Guid ScheduleTravelId, DateTime? InTime, DateTime? OutTime)
        {
            try
            {
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@SCHEDULETRAVELID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, ScheduleTravelId));
                ParameterList.Add(DataAccess.GetDBParameter("@OUTTIME", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, OutTime));
                ParameterList.Add(DataAccess.GetDBParameter("@INTIME", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, InTime));

                DataAccess.ExecSPReturnInt("PRUPDATEOUTORINTIME", ParameterList);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Get(int iPageNumber, int iPageSize, ref int iRowCount, ref int iTotalPageCount)
        {
            List<SqlParameter> ParameterList = new List<SqlParameter>();
            ParameterList.Add(DataAccess.GetDBParameter("@PAGENUMBER", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, iPageNumber));
            ParameterList.Add(DataAccess.GetDBParameter("@PAGESIZE", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, iPageSize));
            ParameterList.Add(DataAccess.GetDBParameter("@RESULTCOUNT", SqlDbType.Int, DbConstant.INT, ParameterDirection.Output, iRowCount));
            ParameterList.Add(DataAccess.GetDBParameter("@TOTALPAGECOUNT", SqlDbType.Int, DbConstant.INT, ParameterDirection.Output, iTotalPageCount));
            DataTable dt = DataAccess.ExecSPReturnDataTable("PRSCHEDULETRAVELSEARCH", ParameterList);

            foreach (SqlParameter sp in ParameterList)
            {
                if (sp.Direction == ParameterDirection.Output)
                {
                    if (sp.ParameterName == "@RESULTCOUNT")
                        iRowCount = (int)sp.Value;

                    if (sp.ParameterName == "@TOTALPAGECOUNT")
                        iTotalPageCount = (int)sp.Value;
                }
            }

            return dt;
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }
    }
}