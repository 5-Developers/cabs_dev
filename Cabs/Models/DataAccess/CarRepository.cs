﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class CarRepository : IRepository<CarModel>
    {
        public IEnumerable<CarModel> Get()
        {
            try
            {
                List<CarModel> l = new List<CarModel>();
                List<SqlParameter> ParameterList = new List<SqlParameter>();
                DataTable dt = DataAccess.ExecSPReturnDataTable("PRCARLIST", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new CarModel
                     {
                         CarId = Guid.Parse(row["FLDCARID"].ToString()),
                         CarName = row["FLDCARNAME"].ToString(),
                         Name = row["FLDNAME"].ToString(),
                         CarModelName = row["FLDCARMODEL"].ToString(),
                         PassengerCapacity = (row["FLDCARPASSENGERCAPACITY"] != DBNull.Value) ? int.Parse(row["FLDCARPASSENGERCAPACITY"].ToString()) : 0,
                         RegNo = row["FLDREGISTRATIONNO"].ToString(),
                         Description = row["FLDDESCRIPTION"].ToString(),
                         Message = ""

                     }).ToList();


                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CarModel> Get(CarModel model)
        {
            List<CarModel> l = new List<CarModel>();
            return (IEnumerable<CarModel>)l;
        }

        public CarModel Get(int Id)
        {
            CarModel m = new CarModel();
            return m;
        }

        public CarModel Get(Guid Id)
        {
            CarModel m = new CarModel();
            List<SqlParameter> ParameterList = new List<SqlParameter>();
            ParameterList.Add(DataAccess.GetDBParameter("@CARID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, Id));
            DataTable dt = DataAccess.ExecSPReturnDataTable("PRCAREDIT", ParameterList);
            if (dt.Rows.Count > 0)
            {
                m.CarId = new Guid(dt.Rows[0]["FLDCARID"].ToString());
                m.CarName = dt.Rows[0]["FLDCARNAME"].ToString();
                m.Name = dt.Rows[0]["FLDNAME"].ToString();
                m.CarModelName = dt.Rows[0]["FLDCARMODEL"].ToString();
                m.PassengerCapacity = (dt.Rows[0]["FLDCARPASSENGERCAPACITY"] != DBNull.Value) ? int.Parse(dt.Rows[0]["FLDCARPASSENGERCAPACITY"].ToString()) : 0;
                m.RegNo = dt.Rows[0]["FLDREGISTRATIONNO"].ToString();
                m.Description = dt.Rows[0]["FLDDESCRIPTION"].ToString();
                m.OlCondition = dt.Rows[0]["FLDOVERALLCONDITION"].ToString();
                m.VehicleColor = dt.Rows[0]["FLDCARCOLOR"].ToString();

                if (dt.Rows[0]["FLDCARYEAR"] != DBNull.Value)
                    m.VechileYear = int.Parse(dt.Rows[0]["FLDCARYEAR"].ToString());
            }
            return m;
        }

        public void Save(CarModel model)
        {
            try
            {
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@CARNAME", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.CarName));
                ParameterList.Add(DataAccess.GetDBParameter("@CARMODEL", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, model.CarModelName));
                ParameterList.Add(DataAccess.GetDBParameter("@CARPASSENGERCAPACITY", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, model.PassengerCapacity));
                ParameterList.Add(DataAccess.GetDBParameter("@REGISTRATIONNO", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.RegNo));
                ParameterList.Add(DataAccess.GetDBParameter("@DESCRIPTION", SqlDbType.NVarChar, DbConstant.NVARCHAR_MAX, ParameterDirection.Input, model.Description));

                ParameterList.Add(DataAccess.GetDBParameter("@CARYEAR", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, model.VechileYear));
                ParameterList.Add(DataAccess.GetDBParameter("@CARCOLOR", SqlDbType.VarChar, DbConstant.VARCHAR_50, ParameterDirection.Input, model.VehicleColor));
                ParameterList.Add(DataAccess.GetDBParameter("@OVERALLCONDITION", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, model.OlCondition));

                ParameterList.Add(DataAccess.GetDBParameter("@USERID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, SecurityContext.CurrentSecurityContext.LoginId));

                if (model.hidCarId == "")
                {
                    ParameterList.Add(DataAccess.GetDBParameter("@CARID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Output, DBNull.Value));
                    DataAccess.ExecSPReturnInt("PRINSERTCAR", ParameterList);

                    foreach (SqlParameter sp in ParameterList)
                    {
                        if (sp.Direction == ParameterDirection.Output)
                        {
                            if (sp.ParameterName == "@CARID")
                                model.hidCarId = sp.Value.ToString();
                        }
                    }
                }
                else
                {
                    ParameterList.Add(DataAccess.GetDBParameter("@CARID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, Guid.Parse(model.hidCarId)));
                    DataAccess.ExecSPReturnInt("PRCARUPDATE", ParameterList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }


        public DataTable Get(int iPageNumber, int iPageSize, ref int iRowCount, ref int iTotalPageCount)
        {
            List<SqlParameter> ParameterList = new List<SqlParameter>();
            ParameterList.Add(DataAccess.GetDBParameter("@PAGENUMBER", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, iPageNumber));
            ParameterList.Add(DataAccess.GetDBParameter("@PAGESIZE", SqlDbType.Int, DbConstant.INT, ParameterDirection.Input, iPageSize));
            ParameterList.Add(DataAccess.GetDBParameter("@RESULTCOUNT", SqlDbType.Int, DbConstant.INT, ParameterDirection.Output, iRowCount));
            ParameterList.Add(DataAccess.GetDBParameter("@TOTALPAGECOUNT", SqlDbType.Int, DbConstant.INT, ParameterDirection.Output, iTotalPageCount));
            DataTable dt = DataAccess.ExecSPReturnDataTable("PRCARSEARCH", ParameterList);

            foreach (SqlParameter sp in ParameterList)
            {
                if (sp.Direction == ParameterDirection.Output)
                {
                    if (sp.ParameterName == "@RESULTCOUNT")
                        iRowCount = (int)sp.Value;

                    if (sp.ParameterName == "@TOTALPAGECOUNT")
                        iTotalPageCount = (int)sp.Value;
                }
            }

            return dt;
        }

        public IEnumerable<CarModel> GetAvailableCars()
        {
            try
            {
                List<CarModel> l = new List<CarModel>();
                List<SqlParameter> ParameterList = new List<SqlParameter>();
                DataTable dt = DataAccess.ExecSPReturnDataTable("PRAVAILABLECARLIST", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new CarModel
                     {
                         CarId = new Guid(row["FLDCARID"].ToString()),
                         CarName = row["FLDCARNAME"].ToString(),
                         Name = row["FLDNAME"].ToString(),
                         CarModelName = row["FLDCARMODEL"].ToString(),
                         PassengerCapacity = (row["FLDCARPASSENGERCAPACITY"] != DBNull.Value) ? int.Parse(row["FLDCARPASSENGERCAPACITY"].ToString()) : 0,
                         RegNo = row["FLDREGISTRATIONNO"].ToString(),
                         Message = ""

                     }).ToList();


                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}