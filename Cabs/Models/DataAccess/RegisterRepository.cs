﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class RegisterRepository : IRepository<RegisterModel>
    {
        public IEnumerable<RegisterModel> Get()
        {
            RegisterModel model = new RegisterModel();
            List<RegisterModel> l = new List<RegisterModel>();
            

            DataTable dt = DataAccess.ExecSPReturnDataTable("", null);

            foreach (var dr in dt.AsEnumerable().ToList())
            {
                model.UserName = "";
                l.Add(model);
            }
            
            
            return (IEnumerable<RegisterModel>)l;
        }

        public IEnumerable<RegisterModel> Get(RegisterModel model)
        {
            List<RegisterModel> l = new List<RegisterModel>();
            
            return (IEnumerable<RegisterModel>)l;
        }

        public RegisterModel Get(int Id)
        {
            RegisterModel m = new RegisterModel();
            return m; 
        }

        public RegisterModel Get(Guid Id)
        {
            RegisterModel m = new RegisterModel();
            return m;
        }

        public void Save(RegisterModel model)
        {
            try
            {
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@LOGINID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, model.Id));
                ParameterList.Add(DataAccess.GetDBParameter("@PASSWORD", SqlDbType.NVarChar, DbConstant.NVARCHAR_100, ParameterDirection.Input, model.Password));
                ParameterList.Add(DataAccess.GetDBParameter("@USERNAME", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.UserName));
                ParameterList.Add(DataAccess.GetDBParameter("@USERID", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, model.UserId));
                ParameterList.Add(DataAccess.GetDBParameter("@ROLE", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, ""));

                DataAccess.ExecSPReturnInt("PRUSERREGISTER", ParameterList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }

    }
}