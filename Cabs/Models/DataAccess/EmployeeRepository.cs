﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class EmployeeRepository : IRepository<EmployeeModel>
    {
        //
        // GET: /EmployeeRepository/

        public IEnumerable<EmployeeModel> Get()
        {
            try
            {
                List<EmployeeModel> l = new List<EmployeeModel>();
                List<SqlParameter> ParameterList = new List<SqlParameter>();
                DataTable dt = DataAccess.ExecSPReturnDataTable("[PREMPLOYEELIST]", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new EmployeeModel
                     {
                         EmployeeId = Guid.Parse(row["FLDEMPLOYEEID"].ToString()),
                         Dob = String.Format("{0:dd/MM/yyyy}", row["FLDDOB"]),                     
                         FirstName = row["FLDFIRSTNAME"].ToString(),
                         LastName = row["FLDLASTNAME"].ToString(),
                         Gender = row["FLDGENDER"].ToString(),
                         Remarks = row["FLDREMARKS"].ToString(),
                         Active =row["FLDACTIVEYN"].ToString(),
                         Phone = row["FLDPHONE"].ToString(),
                         Address = row["FLDADDRESS"].ToString(),
                         City = row["FLDCITY"].ToString(),
                         State = row["FLDSTATE"].ToString(),
                         Zip = row["FLDZIP"].ToString(),
                         Message = ""
                     }).ToList();
                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<EmployeeModel> Get(EmployeeModel model)
        {
            List<EmployeeModel> l = new List<EmployeeModel>();

            return (IEnumerable<EmployeeModel>)l;
        }

        public EmployeeModel Get(int Id)
        {
            EmployeeModel m = new EmployeeModel();
            return m;
        }

        public EmployeeModel Get(Guid Id)
        {
            EmployeeModel m = new EmployeeModel();
            return m;
        }

        public void Save(EmployeeModel model)
        {
            try
            {
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@FIRSTNAME", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.FirstName));
                ParameterList.Add(DataAccess.GetDBParameter("@LASTNAME", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, model.LastName));
                ParameterList.Add(DataAccess.GetDBParameter("@GENDER", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.Gender));
                ParameterList.Add(DataAccess.GetDBParameter("@REMARKS", SqlDbType.VarChar, DbConstant.VARCHAR_MAX, ParameterDirection.Input, model.Remarks));
                ParameterList.Add(DataAccess.GetDBParameter("@ACTIVE", SqlDbType.TinyInt, DbConstant.TINYINT, ParameterDirection.Input, model.ActiveValue));
                ParameterList.Add(DataAccess.GetDBParameter("@PHONE", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, model.Phone));
                ParameterList.Add(DataAccess.GetDBParameter("@ADDRESS", SqlDbType.VarChar, DbConstant.NVARCHAR_MAX, ParameterDirection.Input, model.Address));
                ParameterList.Add(DataAccess.GetDBParameter("@CITY", SqlDbType.VarChar, DbConstant.VARCHAR_50, ParameterDirection.Input, model.City));
                ParameterList.Add(DataAccess.GetDBParameter("@STATE", SqlDbType.VarChar, DbConstant.VARCHAR_30, ParameterDirection.Input, model.State));
                ParameterList.Add(DataAccess.GetDBParameter("@ZIP", SqlDbType.VarChar, DbConstant.VARCHAR_30, ParameterDirection.Input, model.Zip));
                ParameterList.Add(DataAccess.GetDBParameter("@DOB", SqlDbType.DateTime, DbConstant.DATETIME, ParameterDirection.Input, model.Dob));
                ParameterList.Add(DataAccess.GetDBParameter("@USERID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, SecurityContext.CurrentSecurityContext.LoginId));

                if (model.hid == string.Empty)
                {
                    DataAccess.ExecSPReturnInt("PRINSERTEMPLOYEE", ParameterList);
                }
                else
                {
                  //  ParameterList.Add(DataAccess.GetDBParameter("@SCHEDULETRAVELID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, Guid.Parse(model.hid)));
                  //  DataAccess.ExecSPReturnInt("PRSCHEDULETRAVELUPDATE", ParameterList);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }

    }
}
