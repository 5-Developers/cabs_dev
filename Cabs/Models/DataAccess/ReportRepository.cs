﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class ReportRepository : IRepository<ReportsModels>
    {
        public IEnumerable<ReportsModels> Get()
        {
            List<ReportsModels> l = new List<ReportsModels>();
            return (IEnumerable<ReportsModels>)l;
        }

        public IEnumerable<ReportsModels> Get(ReportsModels model)
        {
            List<ReportsModels> l = new List<ReportsModels>();
            return (IEnumerable<ReportsModels>)l;
        }

        public ReportsModels Get(int Id)
        {
            ReportsModels m = new ReportsModels();
            return m;
        }

        public ReportsModels Get(Guid Id)
        {
            ReportsModels m = new ReportsModels();
            return m;
        }

        public void Save(ReportsModels model)
        {
            
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }

        public DataTable GetReportData(string ReportType, string Month, string Year, string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            List<SqlParameter> ParameterList = new List<SqlParameter>();
            dt = DataAccess.ExecSPReturnDataTable("PRSCHEDULETRAVELLIST", ParameterList);

            return dt;
        }

        public void InsertReportInfo(string ReportName, string Type, string Path, string FileName, string Ext)
        {
            try
            {
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@REPORTNAME", SqlDbType.VarChar, DbConstant.VARCHAR_200, ParameterDirection.Input, ReportName));
                ParameterList.Add(DataAccess.GetDBParameter("@REPORTFILENAME", SqlDbType.VarChar, DbConstant.VARCHAR_200, ParameterDirection.Input, FileName));
                ParameterList.Add(DataAccess.GetDBParameter("@REPORTTYPE", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, Type));
                ParameterList.Add(DataAccess.GetDBParameter("@REPORTPATH", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, Path));
                ParameterList.Add(DataAccess.GetDBParameter("@REPORTEXT", SqlDbType.VarChar, DbConstant.VARCHAR_10, ParameterDirection.Input, Ext));
                ParameterList.Add(DataAccess.GetDBParameter("@USERID", SqlDbType.UniqueIdentifier, DbConstant.UNIQUEIDENTIFIER, ParameterDirection.Input, SecurityContext.CurrentSecurityContext.LoginId));

                DataAccess.ExecSPReturnInt("PRREPORTINSERT", ParameterList);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}