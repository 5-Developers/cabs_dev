﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class LoginRepository : IRepository<LoginModel>
    {
        public IEnumerable<LoginModel> Get()
        {
            List<LoginModel> l = new List<LoginModel>();

            return (IEnumerable<LoginModel>)l;
        }

        public IEnumerable<LoginModel> Get(LoginModel model)
        {
            try
            {
                List<LoginModel> l = new List<LoginModel>();
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@USERID", SqlDbType.VarChar, DbConstant.VARCHAR_100, ParameterDirection.Input, model.UserName));
                ParameterList.Add(DataAccess.GetDBParameter("@PASSWORD", SqlDbType.NVarChar, DbConstant.NVARCHAR_100, ParameterDirection.Input, model.Password));

                DataTable dt = DataAccess.ExecSPReturnDataTable("PRUSERLOGIN", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new LoginModel
                     {
                         UserName = row["FLDUSERNAME"].ToString(),
                         LoginId = row["FLDLOGINID"].ToString(),
                         Role = row["FLDROLE"].ToString()
                     }).ToList();

                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        public LoginModel Get(int Id)
        {
            LoginModel m = new LoginModel();
            return m;
        }

        public LoginModel Get(Guid Id)
        {
            LoginModel m = new LoginModel();
            return m;
        }

        public void Save(LoginModel model)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }
    }
}