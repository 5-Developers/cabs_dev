﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Cabs.Models.DataAccess
{
    public class MenuRepository : IRepository<MenuModel>
    {
        public IEnumerable<MenuModel> Get()
        {
            List<MenuModel> l = new List<MenuModel>();

            return (IEnumerable<MenuModel>)l;
        }

        public IEnumerable<MenuModel> Get(MenuModel model)
        {
            try
            {
                List<MenuModel> l = new List<MenuModel>();
                List<SqlParameter> ParameterList = new List<SqlParameter>();

                ParameterList.Add(DataAccess.GetDBParameter("@ROLE", SqlDbType.VarChar, DbConstant.VARCHAR_20, ParameterDirection.Input, model.Role));

                DataTable dt = DataAccess.ExecSPReturnDataTable("PRMENU", ParameterList);

                l = (from DataRow row in dt.Rows
                     select new MenuModel
                     {
                         MenuId = Guid.Parse(row["FLDMENUID"].ToString()),
                         MenuName = row["FLDMENUNAME"].ToString(),
                         MenuShortCode = row["FLDMENUSHORTCODE"].ToString(),
                         Url = row["FLDURL"].ToString(),
                         JsFunction = row["FLDJSFUNCTION"].ToString(),
                         ModuleName = row["FLDMODULENAME"].ToString(),
                         Role = row["FLDROLE"].ToString()

                     }).ToList();

                
                return l;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MenuModel Get(int Id)
        {
            MenuModel m = new MenuModel();
            return m;
        }

        public MenuModel Get(Guid Id)
        {
            MenuModel m = new MenuModel();
            return m;
        }

        public void Save(MenuModel model)
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int Id)
        {
        }

        public void Delete(Guid Id)
        {
        }
    }
}