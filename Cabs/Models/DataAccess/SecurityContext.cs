﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cabs.Models.DataAccess
{
    public class SecurityContext
    {
        public SecurityContext()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public Guid LoginId;
        public string UserName;
        public string Role;
        public int ActiveYN;
        public string Mobile;
        public string EmailId;


        public static bool IsSessionActive()
        {
            if (HttpContext.Current.Session["SecurityContext"] == null)
                return false;
            return true;
        }


        public static SecurityContext CurrentSecurityContext
        {
            get
            {
                return (SecurityContext)HttpContext.Current.Session["SecurityContext"];
            }

            set
            {
                HttpContext.Current.Session["SecurityContext"] = value;
            }
        }
    }
}