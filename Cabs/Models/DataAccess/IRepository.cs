﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cabs.Models.DataAccess
{
    public interface IRepository<T>
    {
        IEnumerable<T> Get();
        IEnumerable<T> Get(T model);
        T Get(int Id);
        T Get(Guid Id);
        void Save(T model);
        void Delete(int Id);
        void Delete(Guid Id);
    }
}