﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Globalization;

namespace Cabs.Models
{
    public class CabsModels
    {
        
    }

    public class IndexModels
    {
        public string str;
    }

    public class TravelTimingModels
    {
        public string ScheduleTravelId { get; set; }
        public string TravelDate { get; set; }
        public string Message { get; set; }

        public int? Month { get; set; }
        public int? Year { get; set; }

        [Display(Name = "From Date")]
        public DateTime? FromDate { get; set; }
        [Display(Name = "To Date")]
        public DateTime? ToDate { get; set; }

        public string Time { get; set; }

        public int RowSpan { get; set; }
        public string Details { get; set; }

        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }

        [Display(Name = "Driver Contact Number")]
        public string DriverContactNo { get; set; }
        
        [Display(Name = "Car Name")]
        public string CarName { get; set; }

        [Display(Name = "Registration Number")]
        public string RegNo { get; set; }

        [Display(Name = "From Location")]
        public string FromLocation { get; set; }
        [Display(Name = "To Location")]
        public string ToLocation { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        [Display(Name = "In Time")]
        public string InTime { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        [Display(Name = "Out Time")]
        public string OutTime { get; set; }

        public IEnumerable<SelectListItem> Months
        {
            get
            {
                return DateTimeFormatInfo
                       .InvariantInfo
                       .MonthNames
                       .Select((monthName, index) => new SelectListItem
                       {
                           Value = (index + 1).ToString(),
                           Text = monthName
                       });
            }
        }

    }
}