﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cabs.Models
{
    public class ReportsModels
    {
        public string ReportType { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string FilePath { get; set; }
        public string FileName { get; set; }
    }
}