﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Cabs.Models
{
    public class AccountModels
    {
        
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string Role { get; set; }
        public string LoginId { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }

    public class RegisterModel
    {
        public enum Roles
        {
            Admin = 1,
            Security = 2
        }

        public Guid Id { get; set; }
        public string UserId { get; set; }
        public Roles Role { get; set; }

        public bool IsActive { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
        
    }

    public class MenuModel
    {
        public Guid MenuId { get; set; }
        public string MenuName { get; set; }
        public string MenuShortCode { get; set; }
        public string Url { get; set; }
        public string JsFunction { get; set; }
        public string ModuleName { get; set; }
        public string Role { get; set; }
        public string Message { get; set; }
    }

    public class CarModel
    {
        public Guid CarId { get; set; }

        [Required]
        [Display(Name = "Car name")]
        public string CarName { get; set; }
        public string Name { get; set; }

        [Display(Name = "Model name")]
        public string CarModelName { get; set; }

        [Required]
        [Display(Name = "Registration Number")]
        public string RegNo { get; set; }
         
        [Display(Name="Vehicle Year")]
        public int? VechileYear {get;set;}

        [Display(Name = "Passenger Capacity")]
        public int? PassengerCapacity { get; set; }

        [Display(Name = "Vehicle Color")]
        public string VehicleColor { get; set; }

        [Display(Name = "Overall Condition")]

        public OverallCondition Overall_Condition { get; set; }

        public string OlCondition { get; set; }

        public enum OverallCondition
        {
            Good,
            Average,
            Poor
        }

        public string Description { get; set; }
        public string Message { get; set; }

        public string hidCarId { get; set; }

        public string PNumber { get; set; }
        public string PSize { get; set; }
        public string RowCount { get; set; }
        public string TotalPageCount { get; set; }
    }

    public class ScheduleTravelModel
    {
        public Guid ScheduleTravelId { get; set; }

        [Required]
        [Display(Name = "Travel Date")]
        public DateTime TravelDate { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        [Display(Name = "Travel Time")]
        public DateTime TravelTime { get; set; }
        [Display(Name = "From Location")]
        public string FromLocation { get; set; }
        [Display(Name = "To Location")]
        public string ToLocation { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        [Display(Name = "In Time")]
        public string InTime { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:hh:mm tt}")]
        [DataType(DataType.Time)]
        [Display(Name = "Out Time")]
        public string OutTime { get; set; }

        public int CompleteYn { get; set; }
        public int CancelYn { get; set; }
        public string Status { get; set; }

        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }

        [Display(Name = "Driver Contact Number")]
        public string DriverContactNo { get; set; }

        [Display(Name = "Km Travelled")]
        public int KmTravelled { get; set; }

        public string hid { get; set; }

        public string Remarks { get; set; }
        public string Message { get; set; }
        public Guid CarId { get; set; }
        public string CarName { get; set; }
        public string RegNo { get; set; }
        public Guid UserId { get; set; }

        public string StDate { get; set; }
        public string StTime { get; set; }
        public string KmT { get; set; }

        public string PNumber { get; set; }
        public string PSize { get; set; }
        public string RowCount { get; set; }
        public string TotalPageCount { get; set; }
    }
    public class EmployeeModel
    {
        public Guid EmployeeId { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "DOB")]
        public string Dob { get; set; }
        public string Gender { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        [Display(Name = "Phone")]
        public string Phone { get; set; }
        [Display(Name = "Zip")]
        public string Zip { get; set; }
        [Display(Name = "Active")]
        public string Active { get; set; }
        public bool ActiveValue { get; set; }
        public int Activev { get; set; }
        public string Male { get; set; }
        public string Female { get; set; }
        public int CompleteYn { get; set; }
        public int CancelYn { get; set; }
        public string Status { get; set; }

        public string hid { get; set; }

        public string Remarks { get; set; }
        public string Message { get; set; }

    }
} 