﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using Cabs.Models;
using Cabs.Models.DataAccess;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;

namespace Cabs.Controllers
{
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/

        public ActionResult ReportMaster()
        {
            return View();
        }

        public JsonResult GetReport(string ReportType, string Month, string Year, string FromDate, string ToDate)
        {
            try
            {
                DataTable dt = new DataTable();
                ReportRepository Rr = new ReportRepository();
                dt = Rr.GetReportData(ReportType, Month, Year, FromDate, ToDate);

                string[] columnNames = { "FLDCARNAME", "FLDTRAVELDATE", "FLDTRAVELSTARTTIME", "FLDFROMLOCATION", "FLDTOLOCATION", "FLDINTIME", "FLDOUTTIME" };
                string[] newColumnNames = { "Car Name", "Travel Date", "Travel Time", "From Location", "To Location", "In Time", "Out Time" };

                DataTable tbl = AlterDataTable(dt, columnNames, newColumnNames);

                string strFileName = ReportType + (Year != "" ? "_" + Year : "") + (Month != "" ? "_" + Month : "");
                string strFilePath = Server.MapPath("~") + "Reports\\" + strFileName + "_" + ".pdf";

                CreatePDF(ConvertDataTableToHTML(tbl), TableCss(), strFilePath);
                Rr.InsertReportInfo(strFileName, ReportType, strFilePath, strFileName, ".pdf");

                ReportsModels Rm = new ReportsModels { ReportType = ReportType, FilePath = strFileName +"_" + ".pdf", Year = int.Parse(Year), Month = int.Parse(Month), FileName = strFileName };
                return Json(Rm, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                DataTable dt = new DataTable();
                var Report = dt.Select().ToList();
                return Json(Report, JsonRequestBehavior.AllowGet);
            }
        }

        public static DataTable AlterDataTable(DataTable dt, string[] ColName, string[] ColNewName)
        {
            DataTable dtNew = dt.DefaultView.ToTable(true, ColName);

            int i = 0;
            foreach (string col in ColNewName)
            {
                dtNew.Columns[i].ColumnName = col;
                i += 1;
            }

            return dtNew;
        }

        public static string ConvertDataTableToHTML(DataTable dt)
        {
            string html = "<table>";
            //add header row
            html += "<thead>";
            html += "<tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<th>" + dt.Columns[i].ColumnName + "</th>";
            html += "</tr>";
            html += "</thead>";
            //add rows
            html += "<tbody>";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</tbody>";
            html += "</table>";
            return html;
        }

        public static string TableCss()
        {
            string strCss = @"table { border-collapse: collapse;
                                      border-spacing: 0;
                                      border:0;
                                      margin: .5em 1em 1em 0;
                                      width:100%; 
                                      background: #34495E;
                                      color: #fff;
                                      border-radius: .4em;
                                      overflow: hidden; 
                                } 
                                th, td { padding: 4px 12px;
                                         vertical-align: top;
                                         text-align: left;
                                } 
                                td { background-color:inherit;
                                     border:solid 1px #DDD;
                                } 
                                td *:last-child {
                                        margin-bottom:0;
                                }
                                th { background-color: #ccc;
                                     color: #34495E;
                                     font-weight: bold;
                                }
                                tr.alt th { color:inherit;
                                            background-color: #e0e0e0;
                                }
                                tr:first-of-type th:first-of-type:empty { visibility: hidden;
                                }";
            return strCss;
        }

        public void CreatePDF(string strHtml, string strCss, string strPath)
        {
            Byte[] bytes;
            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var writer = PdfWriter.GetInstance(doc, ms))
                    {
                        doc.Open();

                        var _Html = strHtml;
                        var _Css = strCss;

                        using (var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(_Css)))
                        {
                            using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(_Html)))
                            {
                                iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msHtml, msCss);
                            }
                        }

                        doc.Close();
                    }
                }

                bytes = ms.ToArray();
            }

            System.IO.File.WriteAllBytes(strPath, bytes);
        }
    }
}
