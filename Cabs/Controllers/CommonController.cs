﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cabs.Models;
using Cabs.Models.DataAccess;

namespace Cabs.Controllers
{
    public class CommonController : Controller
    {
        //
        // GET: /Common/

        public ActionResult Index()
        {
            return View();
        }


        public JsonResult MenuList()
        {
            var MenuList = new List<MenuModel>();
            try
            {
                MenuModel m = new MenuModel();
                m.Role = (SecurityContext.IsSessionActive() == true) ? SecurityContext.CurrentSecurityContext.Role : "";
                MenuRepository Ml = new MenuRepository();
                MenuList = Ml.Get(m).ToList();
            }
            catch (Exception ex)
            {
                MenuList.Add(new MenuModel { Message = ex.Message });
                ModelState.AddModelError("", ex.Message);
            }
            return Json(MenuList, JsonRequestBehavior.AllowGet); 
        }

    }
}
