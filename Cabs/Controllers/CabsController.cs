﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using Cabs.Models;
using Cabs.Models.DataAccess;

namespace Cabs.Controllers
{
    public class CabsController : Controller
    {
        //
        // GET: /Cabs/

        public ActionResult Account()
        {
            if (SecurityContext.IsSessionActive() == true)
            {
                try
                {
                    return View();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    LoginRepository Lr = new LoginRepository();
                    if (Lr.Get(model).Count() > 0)
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        var vlog = Lr.Get(model).First();

                        SecurityContext psc = new SecurityContext();
                        psc.LoginId = new Guid(vlog.LoginId);
                        psc.UserName = vlog.UserName;
                        psc.Role = vlog.Role;
                        SecurityContext.CurrentSecurityContext = psc;
                        return RedirectToAction("Account", "Cabs");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }

        public ActionResult TravelTimings()
        {
            return View();
        }

        public ActionResult TravelDetails()
        {
            return View();
        }

        public JsonResult TravelTimeList(string Month, string Year, string FromDate, string ToDate)
        {
            var TimeList = new List<TravelTimingModels>();
            if (ModelState.IsValid)
            {
                try
                {
                    TravelTimingRepository Tr = new TravelTimingRepository();
                    TravelTimingModels Tm = new TravelTimingModels();
                    if (Month != "")
                        Tm.Month = int.Parse(Month);
                    if (Year != "")
                        Tm.Year = int.Parse(Year);
                    if (FromDate != "")
                        Tm.FromDate = DateTime.Parse(FromDate);
                    if (ToDate != "")
                        Tm.ToDate = DateTime.Parse(ToDate);
                    TimeList = Tr.Get(Tm).ToList();
                }
                catch (Exception ex)
                {
                    TimeList.Add(new TravelTimingModels { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(TimeList, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Cabs/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Cabs/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Cabs/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cabs/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Cabs/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cabs/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Cabs/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
