﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Data;
using Cabs.Models;
using Cabs.Models.DataAccess;


namespace Cabs.Controllers
{
    public class AccountController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult EmployeeMaster()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    LoginRepository Lr = new LoginRepository();
                    if (Lr.Get(model).Count() > 0)
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, false);
                        var vlog = Lr.Get(model).First();

                        SecurityContext psc = new SecurityContext();
                        psc.LoginId = new Guid(vlog.LoginId);
                        psc.UserName = vlog.UserName;
                        psc.Role = vlog.Role;
                        SecurityContext.CurrentSecurityContext = psc;
                        return RedirectToAction("Account", "Cabs");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Cookies.Clear();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult MyAccount(string page, string id)
        {
            //if (Request.IsAuthenticated && SecurityContext.IsSessionActive() == true)
            if (SecurityContext.IsSessionActive() == true)
            {
                try
                {
                    MenuModel m = new MenuModel();
                    m.Role = (SecurityContext.IsSessionActive() == true) ? SecurityContext.CurrentSecurityContext.Role : "";

                    MenuRepository Mr = new MenuRepository();
                    ViewBag.Menu = Mr.Get(m);

                    CarRepository Cr = new CarRepository();
                    ViewBag.CarList = Cr.Get();

                    if (page == null && id != null)
                    {
                        ViewData["PartialView"] = id;
                    }

                    if (page != null && id != null)
                    {
                        ViewData["PartialView"] = page;
                    }

                    return View();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            
        }

        public ActionResult Register()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    RegisterRepository Rr = new RegisterRepository();
                    model.IsActive = true;
                    model.Id = Guid.NewGuid();
                    model.UserId = model.UserName;
                    Rr.Save(model);
                    return RedirectToAction("Login", "Account");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }
            return View(model);
        }


        public JsonResult InsertCar(string CarId, string CarName, string CarModel, string PassengerCapacity, string RegistrationNo, string Description,
                                    string VehicleColor, string VechileYear, string Overall_Condition)
        {
            var CarList = new List<CarModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    if (string.IsNullOrEmpty(RegistrationNo.Trim()) || string.IsNullOrEmpty(CarName.Trim()))
                    {
                        CarList.Add(new CarModel { Message = (string.IsNullOrEmpty(RegistrationNo.Trim()) ? "Registration Number is Required." : "") + "  " + (string.IsNullOrEmpty(CarName.Trim()) ? "Car Name is Required." : "") });
                    }
                    else
                    {
                        CarModel model = new CarModel();
                        CarRepository Cr = new CarRepository();
                        model.CarId = Guid.NewGuid();
                        model.CarName = CarName;
                        model.CarModelName = CarModel;
                        if (PassengerCapacity != "")
                            model.PassengerCapacity = int.Parse(PassengerCapacity);
                        model.Description = Description;
                        model.RegNo = RegistrationNo.Trim();

                        if (VechileYear != "")
                            model.VechileYear = int.Parse(VechileYear);

                        model.VehicleColor = VehicleColor;
                        model.OlCondition = Overall_Condition;

                        model.hidCarId = CarId;

                        Cr.Save(model);

                        CarList.Add(new CarModel { Message = "", hidCarId = model.hidCarId });
                    }
                }
                catch (Exception ex)
                {
                    CarList.Add(new CarModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }

                
            }   
            return Json(CarList, JsonRequestBehavior.AllowGet); 
        }

        public JsonResult EditCar(Guid CarId)
        {
            var CarDetails = new CarModel();
            var stList = new List<CarModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    CarRepository Cr = new CarRepository();
                    CarDetails = Cr.Get(CarId);

                    stList.Add(new CarModel
                    {
                        CarId = CarDetails.CarId,
                        CarName = CarDetails.CarName,
                        Name = CarDetails.Name,
                        CarModelName = CarDetails.CarModelName,
                        PassengerCapacity = CarDetails.PassengerCapacity,
                        RegNo = CarDetails.RegNo,
                        Description = CarDetails.Description,
                        OlCondition = CarDetails.OlCondition,
                        VehicleColor = CarDetails.VehicleColor,
                        VechileYear = CarDetails.VechileYear, 
                        Message = ""
                    });
                }
                catch (Exception ex)
                {
                    stList.Add(new CarModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(stList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListCar()
        {
            var CarList = new List<CarModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    CarRepository Cr = new CarRepository();
                    CarList = Cr.Get().ToList();
                }
                catch (Exception ex)
                {
                    CarList.Add(new CarModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(CarList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AvailableCarList()
        {
            var CarList = new List<CarModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    CarRepository Cr = new CarRepository();
                    CarList = Cr.GetAvailableCars().ToList();
                }
                catch (Exception ex)
                {
                    CarList.Add(new CarModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(CarList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchCar(int PageNumber, int PageSize)
        {
            var CarList = new List<CarModel>();
            if (ModelState.IsValid)
            {
                try
                {  
                    int iRowCount = 0;
                    int iTotalPageCount = 10;
                    CarRepository Cr = new CarRepository();
                    DataTable dt = Cr.Get(PageNumber, PageSize, ref iRowCount, ref iTotalPageCount);
                    ViewBag.PSize = PageSize.ToString();
                    ViewBag.PNumber = PageNumber.ToString();
                    ViewBag.RowCount = iRowCount.ToString();
                    ViewBag.TotalPageCount = iTotalPageCount.ToString();
                    CarList = (from DataRow row in dt.Rows
                               select new CarModel
                               {
                                   CarId = Guid.Parse(row["FLDCARID"].ToString()),
                                   CarName = row["FLDCARNAME"].ToString(),
                                   Name = row["FLDNAME"].ToString(),
                                   CarModelName = row["FLDCARMODEL"].ToString(),
                                   PassengerCapacity = (row["FLDCARPASSENGERCAPACITY"] != DBNull.Value) ? int.Parse(row["FLDCARPASSENGERCAPACITY"].ToString()) : 0,
                                   RegNo = row["FLDREGISTRATIONNO"].ToString(),
                                   Description = row["FLDDESCRIPTION"].ToString(),
                                   TotalPageCount = iTotalPageCount.ToString(),
                                   PNumber = PageNumber.ToString(),
                                   Message = ""
                               }).ToList();
                }
                catch (Exception ex)
                {
                    CarList.Add(new CarModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(CarList, JsonRequestBehavior.AllowGet); 
        }

        public JsonResult InsertScheduleTravel(string ScheduleTravleId, Guid CarId, string TravelDate, string TravelTime, string FromLocation, string ToLocation, string DName, string DContactNo, string Remarks)
        {
            var ScheduleTravel = new List<ScheduleTravelModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    ScheduleTravelModel model = new ScheduleTravelModel();
                    ScheduleTravelRepository Sr = new ScheduleTravelRepository();
                    model.CarId = CarId;
                    model.TravelDate = Convert.ToDateTime(TravelDate);
                    model.TravelTime = Convert.ToDateTime(TravelDate + " " + (TravelTime == "" ? "00:00" : TravelTime));
                    model.FromLocation = FromLocation;
                    model.ToLocation = ToLocation;
                    model.DriverName = DName;
                    model.DriverContactNo = DContactNo;
                    model.Remarks = Remarks;
                    model.hid = ScheduleTravleId;
                    Sr.Save(model);
                    ScheduleTravel.Add(new ScheduleTravelModel { Message = "" });
                }
                catch (Exception ex)
                {
                    ScheduleTravel.Add(new ScheduleTravelModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }


            }
            return Json(ScheduleTravel, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ScheduleTravelList()
        {
            var StList = new List<ScheduleTravelModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    ScheduleTravelRepository Sr = new ScheduleTravelRepository();
                    StList = Sr.Get().ToList();
                }
                catch (Exception ex)
                {
                    StList.Add(new ScheduleTravelModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(StList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ScheduleTravelSearch(int PageNumber, int PageSize)
        {
            var stList = new List<ScheduleTravelModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    int iRowCount = 0;
                    int iTotalPageCount = 10;
                    ScheduleTravelRepository Sr = new ScheduleTravelRepository();
                    DataTable dt = Sr.Get(PageNumber, PageSize, ref iRowCount, ref iTotalPageCount);
                    ViewBag.PSize = PageSize.ToString();
                    ViewBag.PNumber = PageNumber.ToString();
                    ViewBag.RowCount = iRowCount.ToString();
                    ViewBag.TotalPageCount = iTotalPageCount.ToString();
                    stList = (from DataRow row in dt.Rows
                               select new ScheduleTravelModel
                               {
                                   ScheduleTravelId = Guid.Parse(row["FLDSCHEDULETRAVELID"].ToString()),
                                   StDate = String.Format("{0:dd/MM/yyyy}", row["FLDTRAVELDATE"]),
                                   StTime = String.Format("{0:HH:mm}", row["FLDTRAVELSTARTTIME"]),
                                   FromLocation = row["FLDFROMLOCATION"].ToString(),
                                   ToLocation = row["FLDTOLOCATION"].ToString(),
                                   KmT = row["FLDKMTRAVELLED"].ToString(),
                                   Remarks = row["FLDREMARKS"].ToString(),
                                   InTime = row["FLDINTIME"].ToString(),
                                   OutTime = row["FLDOUTTIME"].ToString(),
                                   Status = row["FLDSTATUS"].ToString(),
                                   CarName = row["FLDCARNAME"].ToString(),
                                   DriverName = row["FLDDRIVERNAME"].ToString(),
                                   DriverContactNo = row["FLDDRIVERCONTACTNO"].ToString(),
                                   CarId = Guid.Parse(row["FLDCARID"].ToString()),
                                   TotalPageCount = iTotalPageCount.ToString(),
                                   PNumber = PageNumber.ToString(),
                                   Message = ""
                               }).ToList();
                }
                catch (Exception ex)
                {
                    stList.Add(new ScheduleTravelModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(stList, JsonRequestBehavior.AllowGet); 
        }


        public JsonResult ScheduleTravelEdit(Guid ScheduleTravelId)
        {
            ScheduleTravelModel st = new ScheduleTravelModel();
            var stList = new List<ScheduleTravelModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    ScheduleTravelRepository Sr = new ScheduleTravelRepository();
                    st = Sr.Get(ScheduleTravelId);
                    stList.Add(new ScheduleTravelModel
                    {
                        ScheduleTravelId = st.ScheduleTravelId,
                        StDate = st.StDate,
                        StTime = st.StTime,
                        FromLocation = st.FromLocation,
                        ToLocation = st.ToLocation,
                        KmT = st.KmT,
                        Remarks = st.Remarks,
                        InTime = st.InTime,
                        OutTime = st.OutTime,
                        Status = st.Status,
                        CarName = st.CarName,
                        RegNo = st.RegNo,
                        DriverName = st.DriverName,
                        DriverContactNo = st.DriverContactNo,
                        CarId = st.CarId,
                        Message = ""
                    });
                }
                catch (Exception ex)
                {
                    stList.Add(new ScheduleTravelModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(stList, JsonRequestBehavior.AllowGet);
        }

        public string ScheduleTravelUpdateInandOutTime(Guid ScheduleTravelId, bool IsInTime, bool IsOutTime)
        {
            try
            {
                ScheduleTravelRepository str = new ScheduleTravelRepository();

                if (!IsInTime)
                    str.SaveInOutTime(ScheduleTravelId, null, DateTime.Now);
                else
                    str.SaveInOutTime(ScheduleTravelId, DateTime.Now, null);

                return "Time Updated Successfully.";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public JsonResult InsertEmployee(string Employeeid, string FirstName, string LastName, string Gender, string Dob, string Phone, string ActiveValue, string Address, string City, string State, string Zip, string Remarks)
        {
            var Employee = new List<EmployeeModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    EmployeeModel model = new EmployeeModel();
                    EmployeeRepository Sr = new EmployeeRepository();
                    model.FirstName = FirstName;
                    model.LastName = LastName;
                    model.Gender = Gender;
                    model.Remarks = Remarks;
                    if (model.ActiveValue == true)
                    {
                        model.Activev = 1;
                    }
                    else
                    {
                        model.Activev = 0;
                    }
                    model.Phone = Phone;
                    model.Address = Address;
                    model.City = City;
                    model.State = State;
                    model.Zip = Zip;
                    model.Dob = Dob;
                    model.Remarks = Remarks;
                    model.hid = Employeeid;
                    model.EmployeeId = Guid.NewGuid();
                    Sr.Save(model);
                    Employee.Add(new EmployeeModel { Message = "" });
                }
                catch (Exception ex)
                {
                    Employee.Add(new EmployeeModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }


            }
            return Json(Employee, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EmployeeList()
        {
            var EmpList = new List<EmployeeModel>();
            if (ModelState.IsValid)
            {
                try
                {
                    EmployeeRepository Er = new EmployeeRepository();
                    EmpList = Er.Get().ToList();
                }
                catch (Exception ex)
                {
                    EmpList.Add(new EmployeeModel { Message = ex.Message });
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return Json(EmpList, JsonRequestBehavior.AllowGet);

        }

    }
}
