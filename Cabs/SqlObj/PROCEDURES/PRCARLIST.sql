CREATE PROCEDURE PRCARLIST
AS
BEGIN

	SELECT FLDCARID
			,FLDCARNAME
			,FLDCARMODEL
			,(FLDCARNAME + ' ' + FLDCARMODEL) AS FLDNAME
			,FLDCARPASSENGERCAPACITY
			,FLDREGISTRATIONNO
			,FLDDESCRIPTION
			,FLDACTIVEYN
			,FLDCARCOLOR
			,FLDCARYEAR
			,FLDOVERALLCONDITION
			,FLDCREATEDUSERID
			,FLDCREATEDATE
			,FLDMODIFIEDUSERID
			,FLDMODIFIEDDATE
	FROM TBLCARS (NOLOCK)
	ORDER BY FLDCARNAME
		
END